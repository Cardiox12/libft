# libft

The 42 libft, a simple premise of a fabulous adventure. 

libft structure
-
- libft 
	- ./srcs
		- contains all the source files and will be added to Makefile
	- ./includes
		- contains all the header required for libft
	- ./tests
		- contains a set of unittest for libft
	- Makefile
	
makefile rules : 
-
- all : 
	- compile all the project
- clean
	- clean object file 
- fclean
	- clean object file and executable
- re
	- clean object file and executable and recompile all the project
- test
	- test the libft project with unittest

*Note: the functions will be gradually added to the README file*

unittest : 
-
- The unittest will be separated in two parts : 
 - unittest
	- no supervision needed
 - I/O test 
  	- supervision needed

libft references :
-
- ft_putchar
```c
 	void		ft_putchar(char c);
```	 
- ft_putstr
```c
	void		ft_putstr(char *str);
```
- ft_strcmp
```c
	int		ft_strcmp(char *s1, char *s2);
```
- ft_strlen
```c
	int		ft_strlen(char *str);
``` 
- ft_swap
```c
	void		ft_swap(int *a, int *b);
``` 
- ft_putnbr
```c
	void		ft_putnbr(int nbr);
```
- ft_memset
```c
	void		*ft_memset(void *b, int c, size_t len);
```
- ft_bzero
```c
	void		ft_bzero(void *s, size_t n);
```
- ft_memcpy
```c
	void		*ft_memcpy(void *dst, const void *src, size_t len);
```
- ft_memmove
```c
	void		*ft_memmove(void *dst, const void *src, size_t len);
```
- ft_memcmp
```c
	int		ft_memcmp(const void *s1, const void *s2, size_t n);
```
- ft_strdup
```c
	char    	*ft_strdup(const char *s1);
```
- ft_strcpy
```c
	char		*ft_strcpy(char * dst, const char * src);
``` 
- ft_strncpy
```c
	char 		*ft_strncpy(char *dst, const char *src, size_t len);
```
- ft_strcat
```c
	char		*ft_strcat(char *s1, const char *s2);
``` 
- ft_strcat
```c
	char		*ft_strncat(char *s1, const char *s2, size_t n);
``` 
- ft_strchr
```c
	char		*ft_strchr(const char *s, int c);
```
- ft_strrchr
```c
	char		*ft_strrchr(const char *s, int c);
```
- ft_strstr
```c
	char		*ft_strstr(const char *haystack, const char *needle);
```
- ft_atoi
```c
	int			*ft_atoi(const char *str);
``` 
- ft_isalpha
```c
	int			ft_isalpha(int c);
```
- ft_isdigit
```c
	int			ft_isdigit(int c);
```
- ft_isalnum
```c
	int			ft_isalnum(int c);
```
- ft_isascii
```c
	int			ft_isascii(int c);
```
- ft_isprint
```c
	int			ft_isprint(int c);
```
- ft_toupper
```c
	int			ft_toupper(int c);
```
- ft_tolower
```c
	int			ft_tolower(int c);
```
- ft_islower
```c
	int			ft_islower(int c);
```
- ft_isupper
```c
	int			ft_isupper(int c);
```
- ft_memalloc
```c
	void		*ft_memalloc(void *);
```
- ft_memdel
```c
	void		ft_memdel(void **ap);
```
- ft_strnew
```c
	char		*ft_strnew(size_t size);
```
- ft_strdel
```c
	char		ft_strdel(char **as);
```
- ft_strclr
```c
	char		ft_strclr(char *s);
```
- ft_strjoin
```c
	char 		*ft_strjoin(char const *s1, char const *s2);
```
- ft_striter
```c
	void 		ft_striter(char *s, void (*f)(char *));
```
- ft_striteri
```c
	void 		ft_striteri(char *s, void (*f)(unsigned int, char *));
```
- ft_strmap
```c
	char 		*ft_strmap(char const *s, char (*f)(char));
```
- ft_strmapi
```c
	char		*ft_strmapi(char const *s, char (*f)(unsigned int, char));
```
- ft_strsub
```c
	char		*ft_strsub(const char *s, unsigned int start, size_t len);
```
- ft_strtrim
```c
	char		*ft_strtrim(const char *s);
```
Git workflow : 
- 
- Pull new branch for every new functions added to srcs
- Only merge if it is fully functionnal, no failure should be accepted on master
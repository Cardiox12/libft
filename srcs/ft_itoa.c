/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: toto <toto@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/04 15:47:15 by toto              #+#    #+#             */
/*   Updated: 2019/04/04 15:54:10 by toto             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char *ft_itoa(int n)
{
    long    reverse_n;
    int     digits[11];
    int     index;
    int     ret_index;
    char    *ret;

    index = 0;
    ret_index = 0;
    reverse_n = n;
    if ((ret = (char *)malloc(sizeof(char) * 12)) == NULL)
        return (NULL);
    if (reverse_n < 0)
        ret[ret_index++] = '-';
    reverse_n = (reverse_n < 0) ? reverse_n * -1 : reverse_n;
    digits[index] = reverse_n % 10;
    while (reverse_n > 10)
    {
        reverse_n = reverse_n / 10;
        digits[++index] = reverse_n % 10;
    }
    while (index >= 0)
        ret[ret_index++] = digits[index--] + '0';
    ret[ret_index] = '\0';
    return (ret);
}
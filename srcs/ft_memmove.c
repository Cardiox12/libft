/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: toto <toto@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/29 19:05:38 by toto              #+#    #+#             */
/*   Updated: 2018/10/31 22:38:06 by toto             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void    *ft_memmove(void *dst, const void *src, size_t len)
{
  const size_t const_len = len;
  const char *tmp = (char*)malloc(sizeof(char) * len);
  if (tmp == NULL)
    return (NULL);
  while (len--)
    *(char*)tmp++ = *(char*)src++;
  len = const_len;
  tmp -= const_len;
  while (len--)
    *(char*)dst++ = *(char*)tmp++;
  return (dst - const_len);
}

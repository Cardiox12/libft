/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: toto <toto@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/26 23:55:41 by toto              #+#    #+#             */
/*   Updated: 2019/03/27 10:44:19 by toto             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char    *ft_strmapi(const char *s, char (*f)(unsigned int, char))
{
    const size_t    len = strlen(s);
    size_t          index;
    char            *fresh_src;

    fresh_src = ft_strnew(len + 1);
    index = 0;
    while (s[index])
    {
        fresh_src[index] = f(index, s[index]);
        index++;
    }
    fresh_src[len] = '\0';
    return (fresh_src);
}
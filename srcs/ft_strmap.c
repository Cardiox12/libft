/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: toto <toto@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/26 11:44:18 by toto              #+#    #+#             */
/*   Updated: 2019/03/27 00:24:31 by toto             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char    *ft_strmap(const char *s, char (*f)(char))
{
    char *fresh_src;
    const size_t len = ft_strlen(s);

    fresh_src = ft_strnew(len + 1);
    while (*s)
        *fresh_src++ = f(*s++);
    return fresh_src - len;
}
/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tony <tony@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/08 03:22:53 by tony              #+#    #+#             */
/*   Updated: 2019/02/08 03:42:51 by tony             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char        *ft_strnew(size_t size)
{
    char *ret;

    ret = (char*)malloc(sizeof(char) * size);
    return (ret == NULL) ? NULL: ft_memset(ret, '\0', size);
}
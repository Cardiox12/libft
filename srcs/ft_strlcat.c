/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: toto <toto@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/07 02:04:41 by tony              #+#    #+#             */
/*   Updated: 2019/03/26 23:46:52 by toto             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t      ft_strlcat(char *dst, const char *src, size_t dstsize)
{
    const size_t srclen = ft_strlen(src);
    const size_t dstlen = ft_strlen(dst);

    if (dstlen == dstsize)
        return (dstsize + srclen);
    else if (srclen < dstsize - dstlen)
        ft_memcpy(dst + dstlen, src, srclen + 1);
    else
    {
        ft_memcpy(dst + dstlen, src, dstsize - 1);
        dst[dstlen + dstsize - 1] = '\0';
    }
    return (dstlen + srclen);
}
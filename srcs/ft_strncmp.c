/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tony <tony@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/05 15:21:53 by toto              #+#    #+#             */
/*   Updated: 2019/02/06 02:01:02 by tony             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int     ft_strncmp(const char *s1, const char *s2, size_t n)
{
    while (n)
    {
        if (*s1 != *s2++)
            return (*(unsigned char*)s1 - *(unsigned char*)--s2);
        if (*s1++ == '\0')
            break;
        n--;
    }
    return (0);
}
/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tony <tony@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/30 15:11:05 by tony              #+#    #+#             */
/*   Updated: 2019/03/30 15:16:10 by tony             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char    *ft_strsub(char const *s, unsigned int start, size_t len)
{
    char *ret;

    ret = (char*)malloc(len + 1);
    if (ret == NULL)
        return (NULL);
    ft_strncpy(ret, s + start, len);
    ret[len] = '\0';
    return ret;
}
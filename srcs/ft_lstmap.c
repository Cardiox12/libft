/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tony <tony@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/04 20:37:14 by tony              #+#    #+#             */
/*   Updated: 2019/04/04 20:40:30 by tony             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list  *ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
    t_list *new_list;
    
    if (lst)
    {
        new_list = f(lst);
        new_list->next = ft_lstmap(lst->next, f);
        return (new_list);
    }
    return (NULL);
}
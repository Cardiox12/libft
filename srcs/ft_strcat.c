/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcat.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: toto <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/21 05:45:45 by toto              #+#    #+#             */
/*   Updated: 2018/11/21 06:13:47 by toto             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char    *ft_strcat(char *s1, const char *s2)
{
    const size_t s1_len = ft_strlen(s1);
    const size_t s2_len = ft_strlen(s2);
    ft_memmove(s1 + s1_len, s2, s2_len+1);
    return (s1);
}
/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: toto <toto@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/31 23:24:13 by toto              #+#    #+#             */
/*   Updated: 2018/11/12 00:28:16 by toto             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char    *ft_strncpy(char *dst, const char *src, size_t len)
{
    const size_t length = ft_strlen(src);
    const size_t diff   = length > 0 ? (length - len): length;
    ft_memcpy(dst, src, len);
    ft_bzero(dst + len, diff);
    return (dst);
}